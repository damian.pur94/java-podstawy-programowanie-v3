package pl.sda.task1;

class Circle {

    private final float diameter;

    Circle(float diameter) {
        this.diameter = diameter;
    }

    double calculateCircuit() {
        return diameter * Math.PI;
    }
}
