package pl.sda.task13;

import java.util.Scanner;

class MainTask13 {

    public static void main(String[] args) {
        String theText = getText();

        if (theText.isEmpty()) {
            System.out.print("Provided an empty text");
            return;
        }

        duplicateWords(theText);
    }

    private static String getText() {
        var scanner = new Scanner(System.in);
        System.out.print("Enter a text: ");
        return scanner.nextLine();
    }

    private static void duplicateWords(String theText) {
        String[] words = theText.split(" ");
        for (String word : words) {
            System.out.print(String.format("%s %s ", word, word));
        }
    }
}
