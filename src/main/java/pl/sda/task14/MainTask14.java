package pl.sda.task14;

import java.util.Scanner;

class MainTask14 {

    public static void main(String[] args) {
        var scanner = new Scanner(System.in);
        System.out.print("Enter the first character: ");
        char firstChar = scanner.next().charAt(0);

        System.out.print("Enter the second character: ");
        char secondChar = scanner.next().charAt(0);

        if (!isSmallLetter(firstChar) || !isSmallLetter(secondChar)) {
            System.out.println("Please provide small letters only!");
            return;
        }

        int charactersBetween = Math.abs(firstChar - secondChar) - 1;
        int a = 10;

        System.out.println(String.format("Between %s and %s there are %s characters", firstChar, secondChar, charactersBetween));
    }

    private static boolean isSmallLetter(char letter) {
        return (letter >= 'a' && letter <= 'z');
    }
}
