package pl.sda.task15;

import java.util.Scanner;

class MainTask15 {

    public static void main(String[] args) {
        int[] numbers = getNumbersFromUser();

        for (int i = 0; i < numbers.length; i++) {
            int currentNumber = numbers[i];
            int numberOccurrences = countOccurrences(currentNumber, numbers);
            boolean wasNumberNotChecked = !wasNumberAlreadyChecked(currentNumber, i, numbers);
            if (wasNumberNotChecked && numberOccurrences >= 2) {
                System.out.println(currentNumber);
            }
        }
    }

    private static boolean wasNumberAlreadyChecked(int currentNumber, int currentNumberIndex, int[] numbers) {
        for (int i = 0; i < currentNumberIndex; i++) {
            if (currentNumber == numbers[i]) {
                return true;
            }
        }
        return false;
    }

    private static int countOccurrences(int number, int[] numbers) {
        int occurrences = 0;
        for (int currentNumber : numbers) {
            if (number == currentNumber) {
                occurrences++;
            }
        }
        return occurrences;
    }

    private static int[] getNumbersFromUser() {
        var scanner = new Scanner(System.in);
        System.out.println("Enter ten numbers");
        int[] numbers = new int[10];

        for (int i = 0; i < numbers.length; i++) {
            System.out.print(String.format("Number %s: ", i + 1));
            numbers[i] = scanner.nextInt();
        }

        return numbers;
    }
}
