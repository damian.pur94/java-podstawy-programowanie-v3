package pl.sda.task17;

import java.time.LocalDate;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import java.util.Scanner;

class MainTask17 {

    public static void main(String[] args) {
        var scanner = new Scanner(System.in);
        System.out.print("Provide next class date: ");
        String nextClassDateAsString = scanner.next();

        var polishFormat = DateTimeFormatter.ofPattern("dd.MM.yyyy");
        LocalDate nextClassDate = LocalDate.parse(nextClassDateAsString, polishFormat);
        LocalDate now = LocalDate.now();

        Period periodToNextClasses = Period.between(now, nextClassDate);

        System.out.println(String.format("%s years", periodToNextClasses.getYears()));
        System.out.println(String.format("%s months", periodToNextClasses.getMonths()));
        System.out.println(String.format("%s days", periodToNextClasses.getDays()));

        //Requires change to LocalDateTime
        //long daysToNextClass = Duration.between(now, nextClassDate).get(ChronoUnit.DAYS);
        //System.out.println(String.format("Days: %s", daysToNextClass));
    }
}
