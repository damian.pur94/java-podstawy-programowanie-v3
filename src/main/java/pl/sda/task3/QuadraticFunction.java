package pl.sda.task3;

class QuadraticFunction {

    private final int a;
    private final int b;
    private final int c;

    QuadraticFunction(int a, int b, int c) {
        this.a = a;
        this.b = b;
        this.c = c;
    }

    double[] calculateSolutions() {
        int delta = b * b - 4 * a * c;
        if (delta < 0) {
            return new double[]{};
        } else if (delta == 0) {
            double x0 = calculateX1(delta); //tutaj moglibyśmy wywołać "calculateX2(delta)" zamiast X1 - nie ma to znaczenia, bo i tak dadzą ten sam rezultat (przecież delta jest 0, więc funkcja ma tylko jeden pierwiastek).
            return new double[]{x0};
        } else {
            double x1 = calculateX1(delta);
            double x2 = calculateX2(delta);
            return new double[]{x1, x2};
        }
    }

    private double calculateX1(int delta) {
        return (-1 * b) - (Math.sqrt(delta) / (2 * a));
    }

    private double calculateX2(int delta) {
        return (-1 * b) + (Math.sqrt(delta) / (2 * a));
    }
}
