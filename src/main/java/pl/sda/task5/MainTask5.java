package pl.sda.task5;

import pl.sda.shared.IntNumber;

import java.util.Scanner;

class MainTask5 {

    public static void main(String[] args) {
        var scanner = new Scanner(System.in);
        System.out.print("Enter a positive number: ");
        int number = scanner.nextInt();

        if (number <= 0) {
            System.out.println("Provided a negative number.");
            return;
        }

        //Zaczynamy od "2", ponieważ sprawdzenie "1" nie ma sensu ("1" nie jest liczbą pierwszą)
        for (int i = 2; i < number; i++) {
            var intNumber = new IntNumber(i);
            if (intNumber.isPrime()) {
                System.out.println(i);
            }
        }
    }
}
