package pl.sda.task6;

class HarmonicSeries {

    private final int number;

    HarmonicSeries(int number) {
        this.number = number;
    }

    @Override
    public String toString() {
        float sum = calculateSum();
        return String.valueOf(sum);
    }

    private float calculateSum() {
        float sum = 0;
        for (int i = 1; i <= number; i++) {
            float currentValue = 1F / i;
            sum += currentValue; //w każdej iteracji pętli zwiększamy wartość sumy o element iterowany (musimy mieć do czego dodać przy pierwszym obrocie, więc dlatego na początku jest ona zerem)
        }
        return sum;
    }
}
