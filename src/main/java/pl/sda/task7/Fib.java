package pl.sda.task7;

class Fib {

    private final int number;
    private final long value;

    Fib(int number) {
        this.number = number;
        value = calculate();
    }

    @Override
    public String toString() {
        return String.format("Fib(%s)=%s", number, value);
    }

    private long calculate() {
        //w tym zadaniu potrzebujemy trzech zmiennych i trzech wartości początkowych, ponieważ ostatnia z nich będzie zależeć od dwóch poprzednich
        long previousMinusOne = 0;
        long previous = 1;
        long newFibo = 1;

        for (int i = 1; i < number; i++) {
            newFibo = previousMinusOne + previous; //nowy wyraz ciągu jest sumą dwóch poprzednich - nic dodać, nic ująć
            previousMinusOne = previous; //obecny wyraz musi ustąpić miejsca i stać się poprzednim...
            previous = newFibo; //... i nie jest w cale poszkodowany, ponieważ on sam awansuje na wyższe miejsce
        }
        return newFibo;
    }
}
