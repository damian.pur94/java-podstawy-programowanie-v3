package pl.sda.task7;

import java.util.Scanner;

class MainTask7 {

    public static void main(String[] args) {
        var scanner = new Scanner(System.in);
        System.out.print("Enter a positive number to calculate Fibo: ");
        int number = scanner.nextInt();

        if (number <= 0) {
            System.out.println("Provided a negative number.");
            return;
        }

        var fib = new Fib(number);
        System.out.println(fib);
    }
}
