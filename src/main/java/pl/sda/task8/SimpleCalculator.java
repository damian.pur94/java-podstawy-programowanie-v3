package pl.sda.task8;

class SimpleCalculator {

    private final float firstNumber;
    private final float secondNumber;
    private final String operation;

    SimpleCalculator(float firstNumber, float secondNumber, String operation) {
        this.firstNumber = firstNumber;
        this.secondNumber = secondNumber;
        this.operation = operation;
    }

    void printResult() {
        float result = calculateResult();
        if (result == Float.MIN_VALUE) {
            System.out.println("Cannot divide by 0");
            return;
        }
        System.out.println(String.format("%s %s %s = %s", firstNumber, operation, secondNumber, result));
    }

    private float calculateResult() {
        float result;
        switch (operation) {
            case "*":
                result = firstNumber * secondNumber;
                break;
            case "+":
                result = firstNumber + secondNumber;
                break;
            case "-":
                result = firstNumber - secondNumber;
                break;
            case "/":
                result = handleDivision();
                break;
            default:
                //this will never happen
                result = Float.MIN_VALUE;
        }
        return result;
    }

    private float handleDivision() {
        if (secondNumber == 0) {
            return Float.MIN_VALUE;
            //docelowo kiedy poznamy wyjątki
            //throw new IllegalArgumentException("Cannot divide by 0!");
        }
        return firstNumber / secondNumber;
    }
}
