package pl.sda.task9;

import java.util.Scanner;

class MainTask9 {

    public static void main(String[] args) {
        var scanner = new Scanner(System.in);
        System.out.print("Enter the wave length: ");
        int waveLength = scanner.nextInt();

        var wave = new Wave(waveLength);
        wave.print();
    }
}
